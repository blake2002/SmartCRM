﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using smartcrm.Models;
using smartcrm.Utilities;
using System.Web.Routing;

namespace smartcrm.Controllers
{
    [Authorization]
    public class CustomerController : BaseController
    {        
        public ActionResult Index(string Keywords,string page)
        {
            CustomerList customers = new CustomerList();

            PagerConfig pconfig = GetPagerConfig(page);                        
            customers.RouterDirectory.Add("Keywords", Keywords);
            
            if (string.IsNullOrEmpty(Keywords))
            {
                customers.Customers = Context.Customer.Where(m => m.IsActive == true && m.CompanyNo == CompanyNo).ToList();
            }
            else
            {
                Keywords = Keywords.Trim();
                customers.Customers = Context.Customer.Where(m => m.IsActive == true && m.CompanyNo == CompanyNo && (m.CustomerNo.Contains(Keywords) || m.CustomerFullName.Contains(Keywords) || m.CustomerShortName.Contains(Keywords) || m.SearchKey.Contains(Keywords))).ToList();
            }
            pconfig.TotalRecord = customers.Customers.Count;
            customers.Customers = customers.Customers.ToPagedList(pconfig.CurrentPage, pconfig.PageSize);
            customers.PagerConfig = pconfig;
            return View(customers);
        }

        [HttpPost]
        [PermissionAttribute(PermissionNo = "P0101", IsJson = true)]
        public JsonResult Add(Customer customer)
        {
            JsonResult result = new JsonResult();
            string errorMessage = string.Empty;
            string newCustomerNo = GetNewIdNumber(IdNumberType.Customer);
            customer.CustomerNo = newCustomerNo;
            customer.CreatedDate = DateTime.Now;
            customer.IsActive = true;
            customer.IsOrganization = true;
            customer.LoginId = GetCustomerLoginId(newCustomerNo);
            customer.CompanyNo = CompanyNo;            

            try
            {
                Context.Customer.AddObject(customer);
                Context.SaveChanges();
                result.Data = new { code = 0 };
            }
            catch (Exception ex)
            {
                PutbackNo(newCustomerNo, IdNumberType.Customer);
                throw ex;
            }

            return result;
        }

        [HttpPost]
        [PermissionAttribute(PermissionNo = "P0102", IsJson = true)]
        public JsonResult Update(Customer customer)
        {
            JsonResult result = new JsonResult();
            string errorMessage = string.Empty;

            Customer OriginalCustomer = Context.Customer.FirstOrDefault(m => m.CustomerNo == customer.CustomerNo);
            if (OriginalCustomer == null)
            {
                result.Data = new { code = 1, message = MSG_NoEntityFound };
            }
            else
            {
                OriginalCustomer.CustomerFullName = customer.CustomerFullName;
                OriginalCustomer.CustomerShortName = customer.CustomerShortName;
                OriginalCustomer.Address = customer.Address;
                OriginalCustomer.Phone = customer.Phone;
                OriginalCustomer.Description = customer.Description;
                OriginalCustomer.SearchKey = customer.SearchKey;
                OriginalCustomer.CanLogin = customer.CanLogin;

                Context.SaveChanges();
                result.Data = new { code = 0 };
            }

            return result;
        }

        [HttpPost]
        [PermissionAttribute(PermissionNo = "P0103", IsJson = true)]
        public JsonResult Delete(string customerNo)
        {
            JsonResult result = new JsonResult();
            string errorMessage = string.Empty;

            Customer OriginalCustomer = Context.Customer.FirstOrDefault(m => m.CustomerNo == customerNo);
            if (OriginalCustomer == null)
            {
                result.Data = new { code = 1, message = MSG_NoEntityFound };
            }
            else if (IS_DEMO)
            {
                result.Data = new { code = 1, message = MSG_IsDemo };
            }
            else
            {
                if (customerNo == CompanyNo)
                {
                    result.Data = new { code = 1, message = MSG_SystemReserved };
                }
                else
                {
                    OriginalCustomer.IsActive = false;
                    Context.SaveChanges();
                    result.Data = new { code = 0 };
                }
            }

            return result;
        }

        public JsonResult GetCustomer(string customerNo)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            Customer customer = Context.Customer.FirstOrDefault(m => m.CustomerNo == customerNo);
            if (customer == null)
            {
                result.Data = new { code = 1, message = MSG_NoEntityFound };
            }
            else
            {
                Customer ncustomer = new Customer();
                ncustomer.CustomerNo = customer.CustomerNo;
                ncustomer.CustomerFullName = customer.CustomerFullName;
                ncustomer.CustomerShortName = customer.CustomerShortName;
                ncustomer.Description = customer.Description;
                ncustomer.Address = customer.Address;
                ncustomer.Phone = customer.Phone;
                ncustomer.SearchKey = customer.SearchKey;
                ncustomer.CanLogin = customer.CanLogin;
                result.Data = ncustomer;
            }

            return result;
        }

         [PermissionAttribute(PermissionNo = "P0107", IsJson = true)]
        public JsonResult ResetPassword(string CustomerNo)
        {
            JsonResult result = new JsonResult();

            Customer c = Context.Customer.FirstOrDefault(m => m.CustomerNo == CustomerNo && m.CompanyNo == CompanyNo);
            if (c == null)
            {
                result.Data = new { code = 1, message = MSG_NoEntityFound };
            }
            else if (IS_DEMO)
            {
                result.Data = new { code = 1, message = MSG_IsDemo };
            }
            else
            {
                string newPassword = MD5Utility.GetRandomPassword(6);
                string encryptedPassword = MD5Utility.MD5Encrypt(newPassword);
                c.Password = encryptedPassword;
                Context.SaveChanges();
                result.Data = new { code = 0, message = "密码重置成功!<br/>新密码：<span style='color:red;'>" + newPassword + "</span>" };
            }

            return result;
        }

        public string GetCustomerLoginId(string customerNo)
        {
            Random r = new Random();
            return customerNo + r.Next(1, 9).ToString();
        }

    }
}
