-- Clear company

declare @CompanyNo nvarchar(20) 
set @CompanyNo = '10'

delete from BatchOrderPayment
delete from OrderAttachment where OrderNo in (select OrderNo from [Order] where CompanyNo = @CompanyNo)
delete from OrderContribution where OrderNo in (select OrderNo from [Order] where CompanyNo = @CompanyNo)
delete from OrderFlow where OrderNo in (select OrderNo from [Order] where CompanyNo = @CompanyNo)
delete from OrderLog where OrderNo in (select OrderNo from [Order] where CompanyNo = @CompanyNo)
delete from OrderMemo where OrderNo in (select OrderNo from [Order] where CompanyNo = @CompanyNo)
delete from OrderPayment where OrderNo in (select OrderNo from [Order] where CompanyNo = @CompanyNo)

delete from [Order] where CompanyNo = @CompanyNo

delete from PaymentType where CompanyNo = @CompanyNo

delete from ContributionRatio where CompanyNo = @CompanyNo

delete from Factory where CompanyNo = @CompanyNo
delete from Contact where CustomerNo in(select CustomerNo from Customer where CompanyNo = @CompanyNo)
delete from Customer where CompanyNo = @CompanyNo

delete from NoPool where CompanyNo = @CompanyNo
delete from PoolRestore where CompanyNo = @CompanyNo

delete from UserDepartment where UserId in (select UserId from [User] where CompanyNo = @CompanyNo) 
delete from [User] where CompanyNo = @CompanyNo

delete from Department where CompanyNo = @CompanyNo

delete from WorkFlowStep where WorkFlowNo in (select WorkFlowNo from WorkFlow where CompanyNo = @CompanyNo)
delete from WorkFlow where CompanyNo = @CompanyNo 

delete from OrderStatus where CompanyNo = @CompanyNo

delete from Company where CompanyNo = @CompanyNo

--truncate table OrderLog