-- Inser new company

declare @CompanyNo nvarchar(50)
declare @CompanyFullName nvarchar(50)
declare @CompanyShortName nvarchar(50)
declare @DefaultPassword nvarchar(50)

set @CompanyNo = '100'
set @CompanyFullName ='测试公司'
set @CompanyShortName ='测试公司'

-- Company
insert into Company(CompanyNo,CompanyFullName,ComapnyShortName,OrderTemplate,
	NumberOfUserLimit,IsLocked,ServiceStartDate,AttachmentSize,
	AttachmentNumber,AttachmentRoot)
	values(@CompanyNo,@CompanyFullName,@CompanyShortName,'',50,
		0,GETDATE(),50,10,'D:\temp')
update Company set OrderTemplate = (select top 1 SettingValue  from Configuration where SettingName ='DefaultOrderTemplate') where CompanyNo=@CompanyNo

-- Customer 散客
INSERT INTO Customer(CustomerNo,CustomerFullName,CustomerShortName,CompanyNo,[Description],IsOrganization,IsActive,SearchKey,CreatedDate,[Address],Phone,LoginId,CanLogin,[Password])
     VALUES(@CompanyNo + '101','散客','散客',@CompanyNo,null,0,1,'SK',GETDATE(),null,null,@CompanyNo + '1012',0,null)

-- Payment type
insert into PaymentType(PaymentTypeNo,TypeName,CompanyNo)
     values	(@CompanyNo+'101','前台现金',@CompanyNo)			

-- Order status
insert into OrderStatus(StatusNo,StatusName,CompanyNo,IsSystemReversed)
	values	(@CompanyNo+'101','已创建',@CompanyNo,1),
			(@CompanyNo+'999','已完成',@CompanyNo,1)

-- Default password is "HelloWorld"
-- Default permission is ALL
declare @permissions nvarchar(1000)
set @permissions=''
select @permissions = @permissions + ','+ PermissionNo from Permission
set @permissions = substring(@permissions,2,len(@permissions)-1)

select @DefaultPassword = SettingValue  from Configuration where SettingName ='DefaultPassword'
insert into [User](UserId,UserName,CompanyNo,[Password],IsActive,IsAdmin,[Permissions])
	values(@CompanyNo+'10'+'3','管理员',@CompanyNo,@DefaultPassword,1,1,@permissions) 