﻿var isAdd = true; // Status of order detail
var did = "";
var isOrderAdd;
var DetailArray = new Array(); // Store all order details
var orderTemplate = {};
var allCustomer =[];
var WorkFlowIsNull = "";

$(function () {
    
    WorkFlowIsNull = $("#WorkFlowIsNull").val();

    $("#CustomerNo").find("option").each(function(i){
        allCustomer.push({label:this.text,value:this.value});
    });    

    try
    {
       $('#txtCustomer').autocomplete({
            source: allCustomer,
            select: function( a, b,c ) {
                $("#CustomerNo").val(b.item.value);
                    $('#divSearchCustomer').dialog('close');
            }
        });
    }
    catch (e) {
    }

    $("#TransactionDate").attr("readonly", "readonly");
    $("#TransactionDate").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#TransactionDate").val($("#TransactionDateStr").val());

    $("#ExpectDeliveryDate").attr("readonly", "readonly");
    $("#ExpectDeliveryDate").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#totalFactoryAmount").attr("readonly", "readonly");
    $("#totalAmount").attr("readonly", "readonly");

    if ($("#totalAmount").val() == "0") {
        $("#totalAmount").val("");
    }
    if ($("#FactoryAmount").val() == "0") {
        $("#FactoryAmount").val("");
    }

    $("#Amount").inputmask({ 'mask': "9{0,9}.{0,1}9{0,2}", greedy: false });
    $("#FactoryAmount").inputmask({ 'mask': "9{0,9}.{0,1}9{0,2}", greedy: false });

    $("#Number").inputmask({ 'mask': "9{0,9}", greedy: false });
    $("#Price").inputmask({ 'mask': "9{0,9}.{0,1}9{0,2}", greedy: false });
    $("#FactoryPrice").inputmask({ 'mask': "9{0,9}.{0,1}9{0,2}", greedy: false });
    $("#FactoryAmount").inputmask({ 'mask': "9{0,9}.{0,1}9{0,2}", greedy: false });
        
    $('#FactoryNo').change(function () {
        if ($("#FactoryNo").val() == ""){
            $("#FactoryPrice").val("");
             $("#FactoryAmount").val("");
            $("#divFactory").hide();
        }
        else{
            $("#divFactory").show();
        }
    });

    $('#ContactNo').change(function () {
        if ($('#ContactNo').val() != "") {
            $("#Contact").val($("#ContactNo").find("option:selected").text());
        }
    });

    $("#WorkFlowNo").change(function () {
        if ($("#WorkFlowNo").val() == "") {
            $("#divWorkFlow").hide();
            return;
        }
        else {
            if (WorkFlowIsNull=="1"){
                $("#divWorkFlow").show();
            }
            else{
                $("#divWorkFlow").hide();
            }
        }

        $("#WorkFlowStepNo").empty();
        $("#WorkFlowStepNo").append("<option></option>");
        $("#WorkFlowUserId").empty();
        $("#WorkFlowUserId").append("<option></option>");
        $.ajax({
            url: "/Order/GetWorkFlowNextSteps",
            type: 'GET',
            cache: false,
            dataType: 'json',
            async: false,
            data: { WorkFlowNo: $("#WorkFlowNo").val(), WorkFlowStepNo: "0" },
            success: function (result) {
                if (result.code == 0) {
                    for (var r in result.data) {
                        $("#WorkFlowStepNo").append("<option value='" + result.data[r].ItemValue + "'>" + result.data[r].ItemText + "</option>");
                    }
                }
                else {

                }
            },
            error: function (e) {

            }
        });
    });

    $("#WorkFlowStepNo").change(function () {
        $("#WorkFlowUserId").empty();
        $("#WorkFlowUserId").append("<option></option>");
        $("#isEndPoint").val("0");
        if ($("#WorkFlowStepNo").val() == "") {          
            return;
        }

        $.ajax({
            url: "/Order/GetWorkFlowStepUsers",
            type: 'GET',
            cache: false,
            dataType: 'json',
            async: false,
            data: { WorkFlowNo: $("#WorkFlowNo").val(), WorkFlowStepNo: $("#WorkFlowStepNo").val() },
            success: function (result) {
                if (result.code == 0) {
                    $("#isEndPoint").val(result.isEndPoint);
                    for (var r in result.data) {
                        $("#WorkFlowUserId").append("<option value='" + result.data[r].ItemValue + "'>" + result.data[r].ItemText + "</option>");
                    }
                }
                else {

                }
            },
            error: function (e) {

            }
        });
    });

    $('#CustomerNo').change(function () {
        if ($('#CustomerNo').val() == "") {
            return;
        }

        $.ajax({
            url: "/Order/GetContact",
            type: 'GET',
            cache: false,
            data: { customerNo: $('#CustomerNo').val() },
            success: function (result) {

                if (result != null) {
                    $("#ContactNo").empty();
                    $("#ContactNo").append("<option></option>");
                    for (var r in result) {
                        $("#ContactNo").append("<option value='" + result[r].Id + "'>" + result[r].Name + " " + result[r].Phone + "</option>");
                    }
                }
                else {
                }
            },
            error: function (e) {
            }
        });
    });

    $('#divOrderDetail').dialog({
        width: 640,
        autoOpen: false,
        modal: true,       
        buttons: {
        }
    });

   $('#divSearchCustomer').dialog({
        width: 300,
        autoOpen: false,
        modal: true,
        position:['',80],
        buttons: {
        }
    });
    

    $('#btnCancel').click(function () {
        if (confirm("确定要放弃修改吗？")) {
              window.history.go(-1);
        }
    });

    $('#btnDetailCancel').click(function () {
        $('#divOrderDetail').dialog('close');
    });

    $('#btnDetailClose').click(function () {
        $('#divOrderDetail').dialog('close');
    });

    $('#btnDetailSave').click(function () {
        if (checkRequired('divOrderDetail', 'errorMessageDetail') == false) {
            return;
        }
       
        if ($("#FactoryNo").val()!=""){
            var factoryFilled = true;
            if($("#FactoryPrice").val()==""){
                $("#FactoryPrice").addClass("redBorder");
                factoryFilled = false;
            }
            if($("#FactoryAmount").val()==""){
                $("#FactoryAmount").addClass("redBorder");
                factoryFilled = false;
            }
            
            if (!factoryFilled){
                return;
            }
        }     
 
        var temp = false;
       
        if (WorkFlowIsNull == "1"){
            if ($("#WorkFlowNo").val() == "") {
                $("#WorkFlowNo").addClass("redBorder");
                temp = true;
            }
            if ($("#WorkFlowStepNo").val() == "") {
                $("#WorkFlowStepNo").addClass("redBorder");
                temp = true;
            }
            if ($("#isEndPoint").val() != "1") {
                if ($("#WorkFlowUserId").val() == "") {
                    $("#WorkFlowUserId").addClass("redBorder");
                    temp = true;
                }
            }
        }
        if (temp) {
            return;
        }
       

        var newDetail;
        if (isAdd) {
            var date = new Date();
            did = "D" + date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();

            newDetail = CreateNewDetailObject(did);
            DetailArray.push(newDetail);
            GenerateDetailTableRow(newDetail);
            CalculateAmount();
            $('#divOrderDetail').dialog('close');
            $('#tableEditDetail').attr("name", newDetail.did);
        }
        else {
            did = $('#tableEditDetail').attr("name");
            var tr = $('#tdOrderDetail').find("tr[id='" + did + "']");
            newDetail = CreateNewDetailObject(did);
            var orderDetailNo;
            for (var i = 0; i < DetailArray.length; i++) {
                if (DetailArray[i].did == did) {
                    DetailArray.splice(i, 1);
                    tr.find("td:eq(0)").text(newDetail.Content);
                    tr.find("td:eq(1)").text(newDetail.CategoryName);
                    tr.find("td:eq(2)").html(newDetail.Number);
                    tr.find("td:eq(3)").html(newDetail.Amount);
                    tr.find("td:eq(4)").html(newDetail.FactoryAmount);
                    tr.find("td:eq(5)").html(newDetail.ExpectDeliveryDate);
                    break;
                }
            }
            DetailArray.push(newDetail);
            CalculateAmount();
            $('#divOrderDetail').dialog('close');

        }
    });

    $('#btnSave').click(function () {

        if (isOrderAdd) {
            if (checkRequired('divOrder', 'errorMessage') == false) {
                return;
            }
            if (DetailArray.length == 0) {
                $("#divOrder #errorMessage").text("必须包含至少一个明细项目");
                return;
            }

            if (!confirm("确定保存订单吗？")) {
                return;
            }

            $.ajax({
                url: "/Order/Add",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: { BasicInfo: GetBasicInfo(), DetailInfo: GetDetailInfo() },
                success: function (result) {
                    if (result.code == 0) {
                        location.href = "/Order/Index";
                    }
                    else if (result.code == 9){
                        location.href = "/MySpace/Index";
                    }
                    else {
                        $("#divOrder").find(".errorSpan").text(result.message);
                    }
                },
                error: function (e) {
                    $("#divOrder").find(".errorSpan").text('保存失败.');
                }
            });
        }
        else {
            if (!checkRequired('divOrder', 'errorMessage')) {
                return;
            }

            var additonMessage = "";

            if (confirm(additonMessage + '确定保存修改吗？')) {
                $.ajax({
                    url: "/Order/Update",
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    data: { BasicInfo: GetBasicInfo(), DetailInfo: GetDetailInfo()},
                    success: function (result) {
                        if (result.code == 0) {                      
                            location.href = $("#BackURL").val();
                        }      
                        else {
                            $("#divOrder").find(".errorSpan").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#divOrder").find(".errorSpan").text('保存失败.');
                    }
                });
            }
        }
    });
   
    $('#CustomerNo').change();
    $("#ContactNo").val($("#hContactNo").val()); 
    orderTemplate = eval($("#OrderTemplate").val());
    for (var i in orderTemplate) {
        $("#Category").append("<option value=\"" + orderTemplate[i].id + "\">" + orderTemplate[i].text + "</option>");
    }

    $('#Category').change(function () {
        CategoryChange();
    });

    CategoryChange();

    if ($("#IsAdd").val() == "1") {
        isOrderAdd = true;
    }
    else {
        isOrderAdd = false;
        $("#aAddDetail").hide();

        // Retrieve order details
        $.ajax({
            url: "/Order/GetOrderDetail",
            type: 'GET',
            cache: false,
            data: { OrderNo: $("#OrderNo").val() },
            success: function (result) {
                if (result.code == 0) {
                    CreateExistingDetailObject(result.data);
                    GenerateDetailTable();
                }
                else {
                    alert("获取订单信息失败");
                    window.history.back(-1);
                }
            },
            error: function (e) {
                alert("获取订单信息失败");
                window.history.back(-1);
            }
        });
    }


    $("#Price").keyup(function () {
        PriceChange();
    });
    $("#Price").blur(function () {
        PriceChange();
    });
    $("#Price").change(function () {
        PriceChange();
    });

    $("#FactoryPrice").keyup(function () {
        FactoryPriceChange();
    });
    $("#FactoryPrice").blur(function () {
        FactoryPriceChange();
    });
    $("#FactoryPrice").change(function () {
        FactoryPriceChange();
    });

    $("#Number").keyup(function () {
        PriceChange();
        FactoryPriceChange();
    });
    $("#Number").blur(function () {
        PriceChange();
        FactoryPriceChange();
    });
    $("#Number").change(function () {
        PriceChange();
        FactoryPriceChange();
    });
});

function addDetail() {
    isAdd = true;
    did = "";
    clearForm();
    showOrderDetail();

    $("#divOrderDetail").find("input[type='text']").removeAttr("disabled");
    $("#divOrderDetail").find("textarea").removeAttr("disabled");
    $("#divOrderDetail").find("select").removeAttr("disabled");
    $("#btnDetailSave").show();
    $("#btnDetailCancel").show();
    $("#btnDetailClose").hide();

    $("#divWorkFlow").hide();
    $('#FactoryNo').change();
    return false;
}

function editDetail(id, readonly) {
    isAdd = false;
    did = id;
    clearForm();
    showOrderDetail();

    for (var i = 0; i < DetailArray.length; i++) {
        if (DetailArray[i].did == id) {
           
            $('#tableEditDetail').attr("name", DetailArray[i].did);
            $("#Content").val(DetailArray[i].Content);
            $("#Category").val(DetailArray[i].Category);
            $("#ReportCategory").val(DetailArray[i].ReportCategory);             
            $("#Number").val(DetailArray[i].Number);            
            $("#Price").val(DetailArray[i].Price);
            $("#Amount").val(DetailArray[i].Amount);
            $("#FactoryNo").val(DetailArray[i].FactoryNo);
            $("#FactoryPrice").val(DetailArray[i].FactoryPrice);            
            $("#FactoryAmount").val(DetailArray[i].FactoryAmount);            
            $("#ExpectDeliveryDate").val(DetailArray[i].ExpectDeliveryDate);
            $("#Memo").val(DetailArray[i].Memo);
                        
            $("#Category").change();
            SetDetailDetail(DetailArray[i].Detail);

            $("#WorkFlowNo").val(DetailArray[i].WorkFlowNo);
            if (WorkFlowIsNull=="1"){
                $("#WorkFlowNo").change();            
          
                $("#WorkFlowStepNo option[value='" + DetailArray[i].WorkFlowStepNo +"']").attr('selected', true)
                $("#WorkFlowStepNo").change();

                $("#WorkFlowUserId option[value='" + DetailArray[i].WorkFlowUserId +"']").attr('selected', true) 
            }
            else{
                $("#divWorkFlow").hide();
            }
            break;
        }
    }

    if (readonly) {
        $("#divOrderDetail").find("input[type='text']").attr("disabled", "disabled");
        $("#divOrderDetail").find("textarea").attr("disabled", "disabled");
        $("#divOrderDetail").find("select").attr("disabled", "disabled");
        $("#btnDetailSave").hide();
        $("#btnDetailCancel").hide();
        $("#btnDetailClose").show();
    }
    else {
        $("#divOrderDetail").find("input[type='text']").removeAttr("disabled");
        $("#divOrderDetail").find("textarea").removeAttr("disabled");
        $("#divOrderDetail").find("select").removeAttr("disabled");
        $("#btnDetailSave").show();
        $("#btnDetailCancel").show();
        $("#btnDetailClose").hide();

        if (WorkFlowIsNull=="0") {
            $("#WorkFlowNo").attr("disabled", "disabled");
            $("#divWorkFlow").hide();
        }
    }

    $('#FactoryNo').change();
    return false;
}

function SetDetailDetail(obj) {

    ///eval("var objNew=" + obj + ";");
    var objNew = obj;
    for (var i in objNew) {      
        if ($("#"+ i ).length > 0){
            $("#" + i).val(objNew[i]);            
        } 
    }
 }

function showOrderDetail() {
    $('#divOrderDetail').dialog('open');
}

function closeOrder() {
    $('#divOrderDetail').dialog('close');
}

function clearForm() {
    $("#divOrderDetail").find("input").removeClass("redBorder");
    $("#divOrderDetail").find("select").removeClass("redBorder");
    $("#divOrderDetail").find("input[type=text]").val("");
    $("#divOrderDetail").find("select").val("");
    $("#divOrderDetail").find("textarea").val("");
    $("#divOrderDetail").find(".errorSpan").html("");
    $("#divOrderDetail").find("#Category").change();
}

function CalculateAmount() {
    var totalFactoryAmount = 0;
    var totalAmount = 0;

    for (var i in DetailArray) {
        totalFactoryAmount = totalFactoryAmount + DetailArray[i].FactoryAmount;
        totalAmount = totalAmount + DetailArray[i].Amount;
    }
    
    $("#totalAmount").val(totalAmount);

    if (totalFactoryAmount == 0){
        $("#totalFactoryAmount").val("");
    }
    else{
        $("#totalFactoryAmount").val(totalFactoryAmount);
    }

    
}

function GetBasicInfo(){
    var result = {};
    result.OrderNo = $("#OrderNo").val();
    result.OrderName = $("#OrderName").val();
    result.CustomerNo = $("#CustomerNo").val();
    result.ContactNo = $("#ContactNo").val();
    result.Contact = $("#Contact").val();
    result.TransactionDate = $("#TransactionDate").val();

    return JSON.stringify(result);
}

function GetDetailInfo() {
    for (var a in DetailArray) {
        if (DetailArray[a].Detail != null) {
            DetailArray[a].Detail = JSON.stringify(DetailArray[a].Detail);
        }
    }
  
    return JSON.stringify(DetailArray);
}

function CreateNewDetailObject(did) {
    var newDetail = {
        did: did,       
        Content: $("#Content").val(),
        Category: $("#Category").val(),
        CategoryName: GetCategoryName($("#Category").val()),
        ReportCategory:$("#ReportCategory").val(),
        Detail: GetDetailDetail(),
        DetailText:GetDetailDetailText(),
        Number: $("#Number").val(),
        Price: $("#Price").val(),
        Amount: $("#Amount").val(),
        FactoryNo: $("#FactoryNo").val(),
        FactoryPrice: $("#FactoryPrice").val(),   
        FactoryAmount:$("#FactoryAmount").val(),    
        ExpectDeliveryDate: $("#ExpectDeliveryDate").val(),
        Memo: $("#Memo").val(),
        WorkFlowNo:$("#WorkFlowNo").val(),
        WorkFlowStepNo:$("#WorkFlowStepNo").val(),
        WorkFlowUserId:$("#WorkFlowUserId").val()
    };

    newDetail.FactoryAmount = parseFloat(newDetail.FactoryAmount);
    newDetail.Amount = parseFloat(newDetail.Amount);

    if (isNaN(newDetail.FactoryAmount)) {
        newDetail.FactoryAmount = 0;
    }

    if (isNaN(newDetail.Amount)) {
        newDetail.Amount = 0;
    }

    return newDetail;
}

function GetDetailDetail() {
    var result = {};
    
    $("#tableDetail").find("input[id^='f']").each(function (i, t) {
        eval("result." + t.id + "='" + $(t).val() + "';");
    });
    $("#tableDetail").find("select[id^='f']").each(function (i, t) {
        eval("result." + t.id + "='" + $(t).val() + "';");
    });
  
    return result;
}

function GetDetailDetailText() {
    var result = "";
    $("#tableDetail").find("input[id^='f'],select[id^='f']").each(function (i, t) {
        result = result + $(t).parent().prev().html() + ": " + $(t).val() + ", ";
    });
    if (result.length > 2) {
        result = result.substr(0, result.length - 2);
    }
    return result;
}

function CreateExistingDetailObject(result) {

    for (var i in result) {
      
        var date = new Date();
        var did = "D" + date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds() ;
     
        var newDetail = {
            did: did,
            Content: result[i].Content,
            Category: result[i].Category,
            CategoryName: result[i].CategoryName,
            ReportCategory: result[i].ReportCategory,
            Detail: result[i].Detail,
            DetailText:result[i].DetailText,
            Number: result[i].Number,
            Price: result[i].Price,
            Amount: result[i].Amount,
            FactoryNo: result[i].FactoryNo,
            FactoryPrice:result[i].FactoryPrice,
            FactoryAmount: result[i].FactoryAmount,            
            ExpectDeliveryDate: result[i].ExpectDeliveryDate,
            Memo: result[i].Memo,
            WorkFlowNo:result[i].WorkFlowNo
        };

        if (result[i].ExpectDeliveryDate != null) {
            var d = eval('new ' + result[i].ExpectDeliveryDate.substr(1, result[i].ExpectDeliveryDate.length - 2));           
            newDetail.ExpectDeliveryDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
        }

        newDetail.FactoryAmount = parseFloat(newDetail.FactoryAmount);
        if (isNaN(newDetail.FactoryAmount)){
            newDetail.FactoryAmount = 0;
        }
      
        newDetail.Amount = changeTwoDecimal(newDetail.Amount);
        DetailArray.push(newDetail);
    }
    CalculateAmount();
}

function GenerateDetailTable() {  
    for (var i in DetailArray) {
        GenerateDetailTableRow(DetailArray[i]);
    }
}

function GetCategoryName(id) {     
    for (var i in orderTemplate) {
        
        if (orderTemplate[i].id == id) {
            return orderTemplate[i].text;
            break;
        }
    }
    return "";
}

function GenerateDetailTableRow(newDetail) {   
    var tr = document.createElement("tr");
    tr.id = newDetail.did;
    var td1 = document.createElement("td");
    $(td1).html(newDetail.Content);
    tr.appendChild(td1);

    var td2 = document.createElement("td");
    $(td2).html(GetCategoryName(newDetail.Category));  
    tr.appendChild(td2);

    var td3 = document.createElement("td");
    $(td3).html(newDetail.Number);
    tr.appendChild(td3);

    var td4 = document.createElement("td");
    $(td4).html(newDetail.Amount);
    tr.appendChild(td4);

    var td5 = document.createElement("td");
    $(td5).html(newDetail.FactoryAmount);
    tr.appendChild(td5);

    var td6 = document.createElement("td");
    $(td6).html(newDetail.ExpectDeliveryDate);
    tr.appendChild(td6);

    td6 = $("<td></td>");

    var a1;
    if (isOrderAdd) {

        a1 = $("<img src='../../Images/grid/delete.png' title='删除' />"); 
        a1.click(function () {
            if (confirm("确定删除吗？")) {
                $('#tdOrderDetail').find("tr[id=" + newDetail.did + "]").remove();

                for (var i = 0; i < DetailArray.length; i++) {
                    if (DetailArray[i].did == newDetail.did) {
                        DetailArray.splice(i, 1);
                        CalculateAmount();
                        break;
                    }
                }

            }
        });       
        a1.appendTo(td6);
    }

    a1 = $("<img src='../../Images/grid/edit.png' title='编辑' />");
    a1.click(function () {
        editDetail(newDetail.did, false);
    });

    a1.appendTo(td6);

    a1 = $("<img src='../../Images/grid/detail.png' title='查看详情' />");

    a1.click(function () {
        editDetail(newDetail.did, true);
    });
    a1.appendTo(td6);

    td6.appendTo(tr);

    document.getElementById("tdOrderDetail").appendChild(tr);
    $("#ReportCategory").val(newDetail.ReportCategory);
}

function CategoryChange() {
    var table = $("#tableDetail");
    table.find("tr").remove();
   
    var k = 0;
    var tr;
    var td;
    $("<tr class='title'><td colspan='4'>明细</td></tr>").appendTo(table);
    for (var i in orderTemplate) {
        if (orderTemplate[i].id == $('#Category').val()) {

            $("#ReportCategory").val(orderTemplate[i].reportCategory);
            for (var j in orderTemplate[i].fields) {
                k = k + 1;
                if (k % 2 == 1) {
                    tr = $("<tr></tr>")
                }

                td = $("<td class='label'>" + orderTemplate[i].fields[j].label + "</td>");

                td.appendTo(tr);

                td = $("<td></td>");
                var fieldId = "f" + orderTemplate[i].id + orderTemplate[i].fields[j].id;
                if (orderTemplate[i].fields[j].type == 1) {
                    // textbox
                    var textbox = $("<input type='text' id='" + fieldId + "' />");
                    textbox.appendTo(td);

                }
                else if (orderTemplate[i].fields[j].type == 2) {
                    // dropdown list
                    var ddl = $("<select id='" + fieldId + "'></select>");
                    for (var s in orderTemplate[i].fields[j].options) {
                        ddl.append("<option value=\"" + orderTemplate[i].fields[j].options[s] + "\">" + orderTemplate[i].fields[j].options[s] + "</option>");
                    }
                    ddl.appendTo(td);
                }
                else {
                    // TBD
                    // editable dropdownlist
                }
                if (orderTemplate[i].fields[j].isRequired) {
                    $("<span class='redStar'>*</span>").appendTo(td);
                }
                td.appendTo(tr);

                if (k % 2 == 0) {
                    tr.appendTo(table);
                }
            }

            if (k % 2 == 1) {
                td.attr("colspan", "3");
                tr.appendTo(table);
            }

            break;
        }
    }
}

function PriceChange() {
  
    var price = $("#Price").val();
    var number = $("#Number").val();

    if (!isNaN(price) && !isNaN(number)) {
        $("#Amount").val(parseFloat(price) * parseFloat(number));
    }
}

function FactoryPriceChange() {
    var price = $("#FactoryPrice").val();
    var number = $("#Number").val();

    if (!isNaN(price) && !isNaN(number)) {
        $("#FactoryAmount").val(parseFloat(price) * parseFloat(number));
    }
}

function SearchCustomer(){
     $("#txtCustomer").val("");
     $('#divSearchCustomer').dialog('open');
}