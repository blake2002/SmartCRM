﻿$(function () {
    $("#sb").attr("readonly", "readonly");
    $("#se").attr("readonly", "readonly");
    $("#sb").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#se").datepicker({ dateFormat: 'yy-mm-dd' });

    $("#sb,#se").dblclick(function () {
        $(this).val("");
    });

    $("#btnSave").click(function () {
        if (checkRequired('BugReport', 'errorMessage') == false) {
            return false;
        }
        files = $("input[type=file]");
        var failed = false;
        files.each(function () {

            if ($(this).val() != "") {

                var point = $(this).val().lastIndexOf(".");
                if (point <= 0) {
                    $("#errorMessage").text("未知的文件扩展名");
                    failed = true;
                }

                var type = $(this).val().substr(point).toLowerCase();

                if (!(type == ".jpg" || type == ".jpeg" || type == ".bmp" || type == ".png" || type == "gif")) {
                    $("#errorMessage").text("图片格式仅支持jpg,bmp,png和gif");
                    failed = true;
                }
            }
        });

        if (failed) {
            return false;
        }

        if (!confirm("确定保存吗？")) {
            return false;
        }       
        return true;

    });
});


function AddAttachment() {
    if ($("input[type=file]").length > 10) {
        alert("最多只能添加10个附件！");
        return;
    }
    var date = new Date();
    var did = "D" + date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
    
    $("<div><input type='file' id='" + did +"' name='" + did + "'/><img src='../../Images/grid/delete.png' title='删除' onclick=\"DeleteAttachment('" + did + "');\" /> </div>").appendTo($("#tdAttachment")); 
}

function DeleteAttachment(did) { 
    $("input[id=" + did + "]").parent().remove();
}

