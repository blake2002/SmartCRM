delete Permission
delete RoleGroup
insert into RoleGroup values
	('01','客户联系人管理'),
	('02','订单管理'),
	('03','对账单管理'),
	('04','系统管理')	
insert into Permission values
	('P0101','01','创建客户'),
	('P0102','01','编辑客户'),
	('P0103','01','删除客户'),
	('P0104','01','创建联系人'),
	('P0105','01','编辑联系人'),
	('P0106','01','删除联系人'),
	('P0107','01','重置客户密码'),

	('P0201','02','查询订单'),
	('P0202','02','创建订单'),
	('P0203','02','编辑订单'),
	('P0204','02','删除订单'),
	('P0205','02','管理订单支付'),
	('P0206','02','确认审核订单'),

	('P0301','03','客户对账单'),
	('P0302','03','加工商对账单'),

	('P0401','04','更新公司资料'),
	('P0402','04','管理用户'),
	('P0403','04','设置模板'),
	('P0404','04','设置流程'),
	('P0405','04','管理列表值'),
	('P0406','04','管理加工商')

GO

if exists (select SettingName from Configuration where SettingName='DefaultPermission')
begin
	delete from Configuration where SettingName = 'DefaultPermission'
end
insert into Configuration
		values('DefaultPermission','P0101,P0104,P0202') 
GO

update [User] set Permissions=(select SettingValue from Configuration where SettingName='DefaultPermission')
	where (Permissions is null or Permissions='') and IsAdmin=0
GO

declare @permissions nvarchar(1000)
set @permissions=''
select @permissions = @permissions + ','+ PermissionNo from Permission
set @permissions = substring(@permissions,2,len(@permissions)-1)
update [User] set Permissions= @permissions
	where (Permissions is null or Permissions='') and IsAdmin=1
GO