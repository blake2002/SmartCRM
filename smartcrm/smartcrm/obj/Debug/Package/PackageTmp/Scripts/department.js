﻿var DepartmentNo = "";
var trId = "";
var isAdd = true;

function EditDepartment(DepartmentNo) {  
    isAdd = false;

    clearForm(isAdd);

    $.ajax({
        url: "/System/GetDepartment",
        type: 'GET',
        cache: false,
        data: { DepartmentNo: DepartmentNo },
        success: function (result) {
            if (result.code == 0) {
                $("#DepartmentNo").val(result.data.DepartmentNo);
                $("#DepartmentName").val(result.data.DepartmentName);
                $("#DepartmentDescription").val(result.data.DepartmentDescription);
                if (result.data.Participate) {
                    $("#Participate").attr("checked", true);
                }
                else {
                    $("#Participate").attr("checked", false);
                }
                showDepartment();
                $("#DepartmentName").focus();
            }
            else {
                $("#errorMessage").text(result.message);
            }

        },
        error: function (e) {
            $("#errorMessage").text("查找部门信息失败");
        }
    });

    return false;
}

function DeleteDepartment(DepartmentNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeleteDepartment",
        type: 'POST',
        cache: false,
        data: { DepartmentNo: DepartmentNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = '/System/Department';
            }
            else {
                alert('删除失败');
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}


$(function () {

    $('#divDepartment').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnNew').click(function () {
        isAdd = true;
        clearForm(isAdd);
        showDepartment();        
    });

    $('#btnCancel').click(function () {
        $('#divDepartment').dialog('close');
    });


    $('#btnSave').click(function () {

        if (isAdd) {
            if (checkRequired('divDepartment', 'errorMessage') == false) {
                return;
            }

            $.ajax({
                url: "/System/AddDepartment",
                type: 'POST',
                cache: false,
                data: { DepartmentNo: $("#DepartmentNo").val(), DepartmentName: $("#DepartmentName").val(), DepartmentDescription: $("#DepartmentDescription").val(), Participate: $("#Participate").prop("checked") },
                success: function (result) {
                    if (result.code == 0) {
                        closeDepartment();
                        location.href = "/System/Department";
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divDepartment', 'errorMessage')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {              
                $.ajax({
                    url: "/System/UpdateDepartment",
                    type: 'POST',
                    cache: false,
                    data: { DepartmentNo: $("#DepartmentNo").val(), DepartmentName: $("#DepartmentName").val(), DepartmentDescription: $("#DepartmentDescription").val(), Participate: $("#Participate").prop("checked") },
                    success: function (result) {
                        if (result.code == 0) {
                            closeDepartment();
                            location.href = "/System/Department";
                        }
                        else {
                            $("#errorMessage").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessage").text("保存失败.");
                    }
                });
            }
        }
    });
});

function showDepartment() {
    $('#divDepartment').dialog('open');
}

function closeDepartment() {
    $('#divDepartment').dialog('close');
}

function clearForm(isAdd) {
    $("input").removeClass("redBorder");
    $("input[type=text]").val("");
    $("textarea").val("");
    $("input[type=checkbox]").attr("checked", false);
    $(".errorSpan").html("");
    if (isAdd) {
        $("#DepartmentNo").attr("disabled", false);
        $("#DepartmentNo").focus();
        $("#DepartmentNo").val($("#AutoNo").val());
    }
    else {
        $("#DepartmentNo").attr("disabled", true);
        $("#DepartmentName").focus();
    }
   
}