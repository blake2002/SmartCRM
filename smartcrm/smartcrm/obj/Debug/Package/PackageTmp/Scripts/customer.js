﻿var customerNo = "";
var trId = "";
var isAdd = true;

function EditCustomer(customerNo) {  
    isAdd = false;

    clearForm(isAdd);

    $.ajax({
        url: "/Customer/GetCustomer",
        type: 'GET',
        cache: false,
        data: { customerNo: customerNo },
        success: function (result) {
            if (result.CustomerNo != "") {
                $("#CustomerNo").val(result.CustomerNo);
                $("#CustomerFullName").val(result.CustomerFullName);
                $("#CustomerShortName").val(result.CustomerShortName);
                $("#Address").val(result.Address);
                $("#Phone").val(result.Phone);
                $("#SearchKey").val(result.SearchKey);
                $("#Description").val(result.Description);

                if (result.CanLogin) {
                    $("#CanLogin").attr("checked", true);
                }
                else {
                    $("#CanLogin").attr("checked", false);
                }

                showCustomer();
                $("#CustomerFullName").focus();
            }

        },
        error: function (e) {
        }
    });

    return false;
}

function DeleteCustomer(customerNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/Customer/Delete",
        type: 'POST',
        cache: false,
        data: { customerNo: customerNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = '/Customer';
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}

$(function () {

    $('#divCustomer').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });


    $('#divPassword').dialog({
        width: 180,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnNew').click(function () {
        isAdd = true;
        clearForm(isAdd);
        showCustomer();
        $("#CustomerNo").focus();
    });

    $('#btnCancel').click(function () {
        $('#divCustomer').dialog('close');
    });


    $('#btnSave').click(function () {

        if (isAdd) {
            if (checkRequired('divCustomer', 'errorMessage') == false) {
                return;
            }

            if (!confirm("确定保存吗？")) {
                return;
            }

            $.ajax({
                url: "/Customer/Add",
                type: 'POST',
                cache: false,
                data: {
                    CanLogin: $("#CanLogin").prop("checked"),
                    CustomerFullName: $("#CustomerFullName").val(),
                    CustomerShortName: $("#CustomerShortName").val(),
                    Address: $("#Address").val(),
                    Phone: $("#Phone").val(),
                    SearchKey: $("#SearchKey").val(),
                    Description: $("#Description").val(),
                    CustomerNo: $("#CustomerNo").val()
                },
                success: function (result) {
                    if (result.code == 0) {
                        closeCustomer();
                        location.href = "/Customer";
                    }
                    else {
                        $("#errorMessage").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessage").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divCustomer', 'errorMessage')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {
                $.ajax({
                    url: "/Customer/Update",
                    type: 'POST',
                    cache: false,
                    data: {
                        CanLogin: $("#CanLogin").prop("checked"),
                        CustomerFullName: $("#CustomerFullName").val(),
                        CustomerShortName: $("#CustomerShortName").val(),
                        Address: $("#Address").val(),
                        Phone: $("#Phone").val(),
                        SearchKey: $("#SearchKey").val(),
                        Description: $("#Description").val(),
                        CustomerNo: $("#CustomerNo").val()
                    },
                    success: function (result) {
                        if (result.code == 0) {
                            closeCustomer();
                            location.href = "/Customer";
                        }
                        else {
                            $("#errorMessage").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessage").text("保存失败.");
                    }
                });
            }
        }
    });
});

function showCustomer() {
    $('#divCustomer').dialog('open');
}

function closeCustomer() {
    $('#divCustomer').dialog('close');
}

function clearForm(isAdd) {
    
    $("input").removeClass("redBorder");  
    $("input[type=text]").val("");   
    $(".errorSpan").html("");
  
    $("#CustomerNo").attr("readonly", "readonly");
    $("#CustomerNo").addClass("readOnly");

    if (isAdd) {
        $("#CustomerNo").val("自动编号");
    }
    $("#CustomerFullName").focus();

}


function ResetPassword(no,name) {
    if (!confirm('确定要为客户[ ' + name + '] 重置密码吗？')) {
        return;
    }
    $.ajax({
        url: "/Customer/ResetPassword",
        type: 'POST',
        cache: false,
        data: { CustomerNo: no },
        success: function (result) {
            if (result.code == 0) {
                $('#divPassword').dialog('open')
                $("#divPasswordContent").html(result.message);          
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('密码重置失败');
        }
    });

    return false;
}