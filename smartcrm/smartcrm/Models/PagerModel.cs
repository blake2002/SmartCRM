﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace smartcrm.Models
{
    public class PagerModel
    {
        public PagerConfig PagerConfig { get; set; }
        public RouteValueDictionary RouterDirectory { get; set; }

        public PagerModel()
        {
            PagerConfig = new PagerConfig();
            RouterDirectory = new RouteValueDictionary();
        }
    }
}