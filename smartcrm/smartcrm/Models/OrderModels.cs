﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel;
using System.Data.Objects.DataClasses;

namespace smartcrm.Models
{
    public class OrderList:PagerModel
    {
        public List<OrderInfo> Orders { get; set; }
        public string sc { get; set; }  // Customer
        public string Keywords { get; set; }
        public string ss { get; set; }  // Order Status    
        public string sb { get; set; }  // Begin Date
        public string se { get; set; }  // End Date     
        public string sf { get; set; }
        public List<SimpleItemList> DdlCustomer { get; set; }
        public List<SimpleItemList> DdlStatus { get; set; }
        public List<SimpleItemList> DdlPaymentStatus { get; set; }
        public List<SimpleItemList> DdlPaymentType { get; set; }
        public List<SimpleItemList> DdlFactory { get; set; }
        public float TotalAmount { get; set; }
        public float TotalFactoryAmount { get; set; }
        public float TotalPaid { get; set; }
        public float TotalUnpaid { get; set; }
    }

    public class OrderEditModel
    {
        public Order BasicInfo { get; set; }
        public List<OrderDetailModel> DetailInfo { get; set; }
    }

    public class OrderDetailModel
    {
        public string OrderDetailNo { get; set; }
        public string did { get; set; }
        public string Content { get; set; }
        public string Category { get; set; }
        public string CategoryName { get; set; }
        public string ReportCategory { get; set; }
        public string Detail { get; set; }
        public string DetailText { get; set; }
        public int Number { get; set; }
        public float Price { get; set; }
        public float Amount { get; set; }
        public string FactoryNo { get; set; }
        public float? FactoryAmount { get; set; }
        public float? FactoryPrice { get; set; }
        public DateTime? ExpectDeliveryDate { get; set; }
        public string Memo { get; set; }
        public string WorkFlowNo { get; set; }
        public string WorkFlowStepNo { get; set; }
        public string WorkFlowUserId { get; set; }

    }

    public class OrderInfo : Order
    {
        public string History { get; set; }
        public string Css { get; set; }
        public string StatusDescription { get; set; }
        public string PaymentStatusDescription { get; set; }
        public string TransactionDateString { get; set; }
        public string CreatedTimeString { get; set; }
        public string Payment { get; set; }
        public double? Unpaid { get; set; }
        public string FactoryName { get; set; }
        public string ExpectDeliveryDateString { get; set; }
        public List<SimpleItemList> Attachments { get; set; }
        public List<JSOrderWorkFlow> steps { get; set; }
        public List<SimpleItemList> OrderMemos { get; set; }
        public string QueryURL { get; set; }
    }

    public class JSOrderWorkFlow
    {
        public string StepName { get; set; }
        public string UserName { get; set; }
        public string DateTime { get; set; }
        public string Memo { get; set; }
        public bool IsFinished { get; set; }
    }

    public class OrderModel : Order
    {
        public List<SimpleItemList> DdlCustomer { get; set; }

        public List<SimpleItemList> DdlFactory { get; set; }
        public List<SimpleItemList> DdlWorkFlow { get; set; }

        public List<SimpleItemList> DdlOrderStatus { get; set; }
        public List<SimpleItemList> DdlOrderExternalStatus { get; set; }
        public List<SimpleItemList> DdlContact { get; set; }
        public List<SimpleItemList> DdlPaymentType { get; set; }

        public string IsAdd { get; set; }
        public string hContactNo { get; set; }

        public string TransactionDateStr { get; set; }

        public string WorkFlowIsNull { get; set; }

        public string BackURL { get; set; }

        public OrderModel()
        {

        }

        public void BindFromOrderEntity(Order order)
        {
            this.OrderNo = order.OrderNo;
            this.OrderName = order.OrderName;
            this.Contact = order.Contact;
            this.ContactNo = order.ContactNo;
            this.PaymentStatus = order.PaymentStatus;
            this.OrderStatusNo = order.OrderStatusNo;
            this.CustomerNo = order.CustomerNo;
            this.CustomerShortName = order.CustomerShortName;
            this.Amount = order.Amount;
            this.Paid = order.Paid;
            this.Creator = order.Creator;
            this.CreatedTime = order.CreatedTime;
            this.TransactionDate = order.TransactionDate;
            this.IsActive = order.IsActive;
            this.OrderTemplate = order.OrderTemplate;
            this.Memo = order.Memo;
        }
    }

    public enum OrderPaymentStatus
    {
        [Description("未支付")]
        Unpaid = 1, //未支付      
        [Description("部分支付")]
        PartPaid = 2,   //部分支付
        [Description("已支付")]
        Paid = 3    //已支付
    }

    //public enum OrderExternalStatus
    //{
    //    None= 0,    //无
    //    Unpaid = 1, //未支付      
    //    Paid = 2    //已支付
    //}

    public class SimpleItemList
    {
        public SimpleItemList()
        {
        }
        public string ItemValue { get; set; }
        public string ItemText { get; set; }
        public string Addition1 { get; set; }
        public string Addition2 { get; set; }
        public string Addition3 { get; set; }
    }

    public class JsonOrder
    {
        public int OrderDetailNo { get; set; }
        public string OrderNo { get; set; }
        public string Content { get; set; }
        public string Category { get; set; }
        public string CategoryName { get; set; }
        public string ReportCategory { get; set; }
        public string DetailString { get; set; }
        public object Detail { get; set; }
        public string DetailText { get; set; }
        public int Number { get; set; }
        public double Price { get; set; }
        public double Amount { get; set; }
        public string FactoryNo { get; set; }
        public double? FactoryPrice { get; set; }
        public double? FactoryAmount { get; set; }
        public DateTime? ExpectDeliveryDate { get; set; }
        public string WorkFlowNo { get; set; }
        public string Memo { get; set; }
    }

    public class CustomerStatement : OrderList
    {
        public string CustomerName { get; set; }
    }

    public class FactoryStatement
    {
        public string sf { get; set; }
        public string sb { get; set; }
        public string se { get; set; }
        public List<SimpleItemList> DdlFactory { get; set; }
        public List<OrderInfo> Orders { get; set; }
        public double? TotalAmount { get; set; }
    }

    public class JSOrderNextStep
    {
        public List<SimpleItemList> FollowUps { get; set; }
        public List<SimpleItemList> TeamUserIds { get; set; }
        public bool IsEndPoint { get; set; }

        public JSOrderNextStep()
        {
            FollowUps = new List<SimpleItemList>();
            TeamUserIds = new List<SimpleItemList>();
            IsEndPoint = false;
        }
    }

    public class AttachmentConfig
    {
        public float SizeLimitation { get; set; }
        public string RootPath { get; set; }
    }

    public class OrderQueryResult
    {
        public string OrderNo { get; set; }
        public string Status { get; set; }
        public string CustomerName { get; set; }
        public DateTime TransactionDate { get; set; }
        public string OrderDetailText { get; set; }
        public string OrderName { get; set; }
        public int Number { get; set; }
        public List<SimpleItemList> WorkFlows { get; set; }
        public string Message { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }

        public OrderQueryResult()
        {
            WorkFlows = new List<SimpleItemList>();
        }
    }

    public class JsUrl
    {
        public string root { get; set; }
        public List<JsPara> paras { get; set; }
    }
    public struct JsPara
    {
        public string name{get;set;}
        public string value{get;set;}
    }
}
