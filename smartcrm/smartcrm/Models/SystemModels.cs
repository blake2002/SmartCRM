﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel;
using System.Data.Objects.DataClasses;

namespace smartcrm.Models
{
    public class UserListModel
    {
        public List<UserModel> Users { get; set; }
        public List<RoleGroupModel> RoleGroups { get; set; }
    }

    public class RoleGroupModel
    {
        public string RoleGroupNo { get; set; }
        public string RoleGroupName { get; set; }
        public List<PermissionModel> Permissions { get; set; }
    }

    public class PermissionModel
    {
        public string PermissionNo { get; set; }
        public string PermissionName { get; set; }
    }
    public class UserModel : User
    {
        public string Department { get; set; }
    }

    public class ListOption
    {
        public string OptionValue { get; set; }
        public string OptionText { get; set; }
        public string AdditionalValue { get; set; }
    }

    public class WorkFlowDetailModel
    {
        public string WorkFlowName { get; set; }
        public string WorkFlowNo { get; set; }
        public List<WorkFlowStepModel> Steps { get; set; }
        public List<SimpleItemList> DdlOrderStatus { get; set; }
    }

    public class WorkFlowStepModel
    {
        public string WorkFlowStepNo { get; set; }
        public string WorkFlowStepName { get; set; }
        public string WorkFlowNo { get; set; }
        public bool CanMarkComplete { get; set; }
        public string StatusName { get; set; }
        public List<ListOption> Users { get; set; }
        public List<ListOption> FollowUps { get; set; }
        public string StatusNo { get; set; }
        public string SN { get; set; }
        public bool IsSystemReserved { get; set; }
        public bool IsBeginPoint { get; set; }
        public bool IsEndPoint { get; set; }
    }

    public class ListModel
    {
        public List<OrderStatus> OrderStatus { get; set; }
        public List<PaymentType> PaymentTypes { get; set; }
        public List<ContributionRatio> ContributionRatio { get; set; }
    }

    public class IdNumber
    {
        public string No { get; set; }
        public IdNumberType Type { get; set; }
    }
}
  
