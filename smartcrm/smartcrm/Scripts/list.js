﻿var FactoryNo = "";
var trId = "";
var isAdd = true;

function EditPaymentType(PaymentTypeNo) {  
    isAdd = false;
    clearForm(isAdd,"divPaymentType");
    $.ajax({
        url: "/System/GetPaymentType",
        type: 'GET',
        cache: false,
        data: { PaymentTypeNo: PaymentTypeNo},
        success: function (result) {
            if (result.code == 0) {
                $("#TypeName").val(result.data.TypeName);
                $("#PaymentTypeNo").val(result.data.PaymentTypeNo);
                showPaymentType();
            }

        },
        error: function (e) {
        }
    });

    return false;
}

function EditOrderStatus(StatusNo) {
    isAdd = false;
    clearForm(isAdd, "divOrderStatus");
    $.ajax({
        url: "/System/GetOrderStatus",
        type: 'GET',
        cache: false,
        data: { StatusNo: StatusNo },
        success: function (result) {
            if (result.code == 0) {
                $("#StatusNo").val(result.data.StatusNo);
                $("#StatusName").val(result.data.StatusName);
                showOrderStatus();
            }

        },
        error: function (e) {
        }
    });

    return false;
}

function EditContributioRatio(RatioNo) {
    isAdd = false;
    clearForm(isAdd, "divContributioRatio");
    $.ajax({
        url: "/System/GetContributionRatio",
        type: 'GET',
        cache: false,
        data: { RatioNo: RatioNo },
        success: function (result) {
            if (result.code == 0) {
                $("#RatioNo").val(result.data.RatioNo);
                $("#RatioNumber").val(result.data.RatioNumber);
                showContributionRatio();
            }

        },
        error: function (e) {
        }
    });

    return false;
}

function DeletePaymentType(PaymentTypeNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeletePaymentType",
        type: 'POST',
        cache: false,
        data: { PaymentTypeNo: PaymentTypeNo},
        success: function (result) {
            if (result.code == 0) {
                location.href = '/System/List';
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}

function DeleteOrderStatus(StatusNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeleteOrderStatus",
        type: 'POST',
        cache: false,
        data: { StatusNo: StatusNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = '/System/List';
            }
            else {
                alert(result.message);
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}

function DeleteContributionRatio(RatioNo) {
    if (!confirm('确定删除吗？')) {
        return;
    }
    $.ajax({
        url: "/System/DeleteContributionRatio",
        type: 'POST',
        cache: false,
        data: { RatioNo: RatioNo },
        success: function (result) {
            if (result.code == 0) {
                location.href = '/System/List';
            }
            else {
                alert('删除失败');
            }

        },
        error: function (e) {
            alert('删除失败');
        }
    });

    return false;
}

$(function () {

    $('#divPaymentType').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });
    $('#divOrderStatus').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });
    $('#divContributionRatio').dialog({
        width: 500,
        autoOpen: false,
        modal: true,
        buttons: {
        }
    });

    $('#btnPaymentTypeNew').click(function () {
        isAdd = true;
        clearForm(isAdd, "divPaymentType");
        showPaymentType();
    });
    $('#btnOrderStatusNew').click(function () {
        isAdd = true;
        clearForm(isAdd, "divOrderStatus");
        showOrderStatus();
    });
    $('#btnContributionRatioNew').click(function () {
        isAdd = true;
        clearForm(isAdd, "divContributionRatio");
        showContributionRatio();
    });

    $('#btnPaymentTypeCancel').click(function () {
        $('#divPaymentType').dialog('close');
    });
    $('#btnOrderStatusCancel').click(function () {
        $('#divOrderStatus').dialog('close');
    });
    $('#btnContributionRatioCancel').click(function () {
        $('#divContributionRatio').dialog('close');
    });

    $('#btnPaymentTypeSave').click(function () {
        if (isAdd) {
            if (checkRequired('divPaymentType', 'errorMessagePaymentType') == false) {
                return;
            }
            console.log($('#formPaymentType').serialize());
            $.ajax({
                url: "/System/AddPaymentType",
                type: 'POST',
                cache: false,
                data: $('#formPaymentType').serialize(),
                success: function (result) {
                    if (result.code == 0) {
                        closePaymentType();
                        location.href = "/System/List";
                    }
                    else {
                        $("#errorMessagePaymentType").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessagePaymentType").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divPaymentType', 'errorMessagePaymentType')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {
                $.ajax({
                    url: "/System/UpdatePaymentType",
                    type: 'POST',
                    cache: false,
                    data: $('#formPaymentType').serialize(),
                    success: function (result) {
                        if (result.code == 0) {
                            closePaymentType();
                            location.href = "/System/List";
                        }
                        else {
                            $("#errorMessagePaymentType").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessagePaymentType").text("保存失败.");
                    }
                });
            }
        }
    });

    $('#btnOrderStatusSave').click(function () {
        if (isAdd) {
            if (checkRequired('divOrderStatus', 'errorMessageOrderStatus') == false) {
                return;
            }

            $.ajax({
                url: "/System/AddOrderStatus",
                type: 'POST',
                cache: false,
                data: $('#formOrderStatus').serialize(),
                success: function (result) {
                    if (result.code == 0) {
                        closeOrderStatus();
                        location.href = "/System/List";
                    }
                    else {
                        $("#errorMessageOrderStatus").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessageOrderStatus").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divOrderStatus', 'errorMessageOrderStatus')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {
                $.ajax({
                    url: "/System/UpdateOrderStatus",
                    type: 'POST',
                    cache: false,
                    data: $('#formOrderStatus').serialize(),
                    success: function (result) {
                        if (result.code == 0) {
                            closeOrderStatus();
                            location.href = "/System/List";
                        }
                        else {
                            $("#errorMessageOrderStatus").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessageOrderStatus").text("保存失败.");
                    }
                });
            }
        }
    });

    $('#btnContributionRatioSave').click(function () {
        if (isAdd) {
            if (checkRequired('divContributionRatio', 'errorMessageContributionRatio') == false) {
                return;
            }

            $.ajax({
                url: "/System/AddContributionRatio",
                type: 'POST',
                cache: false,
                data: $('#formContributionRatio').serialize(),
                success: function (result) {
                    if (result.code == 0) {
                        closeContributionRatio();
                        location.href = "/System/List";
                    }
                    else {
                        $("#errorMessageContributionRatio").text(result.message);
                    }
                },
                error: function (e) {
                    $("#errorMessageContributionRatio").text("保存失败.");
                }
            });
        }
        else {
            if (!checkRequired('divContributionRatio', 'errorMessageContributionRatio')) {
                return;
            }
            if (confirm('确定保存修改吗？')) {
                $.ajax({
                    url: "/System/UpdateContributionRatio",
                    type: 'POST',
                    cache: false,
                    data: $('#formContributionRatio').serialize(),
                    success: function (result) {
                        if (result.code == 0) {
                            closeContributionRatio();
                            location.href = "/System/List";
                        }
                        else {
                            $("#errorMessageContributionRatio").text(result.message);
                        }
                    },
                    error: function (e) {
                        $("#errorMessageContributionRatio").text("保存失败.");
                    }
                });
            }
        }
    });

});

function showPaymentType() {
    $('#divPaymentType').dialog('open');
}
function closePaymentType() {
    $('#divPaymentType').dialog('close');
}
function showOrderStatus() {
    $('#divOrderStatus').dialog('open');
}
function closeOrderStatus() {
    $('#divOrderStatus').dialog('close');
}
function showContributionRatio() {
    $('#divContributionRatio').dialog('open');
}
function closeContributionRatio() {
    $('#divContributionRatio').dialog('close');
}


function clearForm(isAdd,div) {
    $("#"+div).find("input").removeClass("redBorder");
    $("#" + div).find("input[type=text]").val("");
    $("#" + div).find("textarea").val("");
    $(".errorSpan").html("");
}