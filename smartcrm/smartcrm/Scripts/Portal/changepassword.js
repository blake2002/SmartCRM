﻿$(function () {

    $("#btnSave").click(function () {
        if (checkRequired('divChangePassword', 'errorMessage') == false) {
            return false;
        }
        if ($("#NewPassword1").val() != $("#NewPassword2").val()) {
            $('#errorMessage').html("新密码两次输入不一致");
            return;
        }

        if (!confirm("确定修改密码吗？")) {
            return;
        }

        $.ajax({
            url: "/Portal/UpdatePassword",
            type: 'POST',
            cache: false,
            dataType: 'json',
            async: false,
            data: { OldPassword: $("#OldPassword").val(), NewPassword1: $("#NewPassword1").val(), NewPassword2: $("#NewPassword2").val() },
            success: function (result) {
                if (result.code == 0) {
                    alert("密码修改成功");
                    $("input[type=password]").val("");
                }
                else {
                    $('#errorMessage').html(result.message);
                }
            },
            error: function (e) {
                $('#errorMessage').html('密码修改失败');
            }
        });

    });
});

