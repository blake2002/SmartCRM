﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using System.Diagnostics;
using System.Configuration;
using System.IO;

namespace smartcrm.Utilities
{    
    public class PdfCompanyInformation
    {
        public string Name { get; set; }
        public string LogoPath { get; set; }
        public string Address { get; set; }
        public string PhoneFax { get; set; }
        public string PostCode { get; set; } 

    }
    public class PdfHelper
    {       
        public static string PDF_TOOLS_PATH = ConfigurationManager.AppSettings["PdfToolsPath"].ToString();
       
        public PdfHelper()
        {
        }

        public bool WKHtmlToPdf(string htmlFile, string pdfFile, bool isLandscape)
        {             
            var p = new Process();

            string orientation;
            if (isLandscape)
            {
                orientation = "Landscape";
            }
            else
            {
                orientation = "Portrait";
            }
            var startInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(PDF_TOOLS_PATH, "wkhtmltopdf.exe"),
                Arguments = string.Format(@"""{0}"" ""{1}"" ""{2}"" ""{3}"" ""{4}"" ""{5}"" ""{6}"" ""{7}""", "--footer-right", "[page]/[toPage]", "--page-size", "A4", "--orientation", orientation, htmlFile, pdfFile),
                WindowStyle = ProcessWindowStyle.Hidden
            };
          
            p.StartInfo = startInfo;
            p.Start();

            p.WaitForExit(60000);

            int returnCode = p.ExitCode;
            p.Close();

            // 0: successful
            return returnCode == 0;
        }


    }
}